var express = require('express');

const cors = require('cors');
const morgan = require('morgan');

module.exports = function(config, models) {
	var app = express();

	app.use(cors());
	app.use(morgan('common'));
	app.use(require('./routes')(config, models))

	return app;
};
