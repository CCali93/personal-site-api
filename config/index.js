const _ = require('_');

var configObj = require('./defaults');

configObj = _.assign(configObj, require('./' + process.env.NODE_ENV));

try {
	configObj = _.assign(configObj, require('./local'));
} catch (e) {
	configObj = _.assign(configObj, {});
}

module.exports = configObj;
