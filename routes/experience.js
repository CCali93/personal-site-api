var responseFunctions;

module.exports = function(config, models) {
	if (responseFunctions !== undefined) {
		return responseFunctions;
	}

	responseFunctions = {};

	responseFunctions.getList = function(req, res) {
		res.status(200).json([{
                badgeClass: 'blue darken-3',
                icon: 'grade',
                title: 'Internship at Krowdbyz',
                content: 'First foray into real software development',
                date: moment(now).format('MMM YYYY')
            }, {
                badgeClass: 'orange darken-4',
                icon: 'school',
                title: 'Began attending RIT',
                content: 'First step towards an amazing career',
                date: moment(now).format('MMM YYYY')
            }
        ]);
	};

	return responseFunctions;
};
