const express = require('express');

module.exports = function(config, models) {
	const testimonials = require('./testimonials')(config, models);
	const elevatorPitch = require('./elevatorPitch')(config, models);
	const experience = require('./experience')(config, models);
	const strengths = require('./strengths')(config, models);
	const skills = require('./skills')(config, models);

	var router = express.Router();

	router.get('/testimonials', testimonials.getList);
	router.get('/elevatorPitch', elevatorPitch.get);
	router.get('/experience', experience.getList);
	router.get('/strengths', strengths.getList);
	router.get('/skills', skills.getList);

	return router;
};
