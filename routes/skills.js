var responseFunctions;

module.exports = function(config, models) {
	if (responseFunctions !== undefined) {
		return responseFunctions;
	}

	responseFunctions = {};

	responseFunctions.getList = function(req, res) {
		res.status(200).json([
            {
                name: 'Programming Languages',
                skills: [
                    'Java',
                    'Javascript',
                    'Python',
                    'HTML5',
                    'PHP',
                    'Ruby',
                    'C/C++',
                    'Haskell',
                    'CSS'
                ]
            },
            {
                name: 'Development Tools',
                skills: [
                    'Git',
                    'Linux',
                    'NodeJS',
                    'AngularJS',
                    'BackboneJS',
                    'RequireJS',
                    'Redis',
                    'Amazon Web Services',
                    'Grunt',
                    'D3',
                    'Ruby on Rails',
                    'Ruby Chef',
                    'Docker'
                ]
            },
            {
                name: 'Databases',
                skills: ['MySQL', 'SQLite', 'Oracle', 'MongoDB']
            },
            {
                name: 'Operating Systems',
                skills: [
                    'Windows (XP and Above)',
                    'Ubuntu Linux',
                    'Mac OS X',
                    'Android'
                ]
            },
            {
                name: 'Miscellaneous',
                skills: [
                    'Online project collaboration',
                    'Scrum',
                    'Agile Development',
                    'Functional Programming',
                    'Object Oriented Programming',
                    'Web Development'
                ]
            }
        ]);
	};

	return responseFunctions;
};
