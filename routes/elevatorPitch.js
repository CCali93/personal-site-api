var responseFunctions;

module.exports = function(config, models) {
	if (responseFunctions !== undefined) {
		return responseFunctions;
	}

	responseFunctions = {};

	responseFunctions.get = function(req, res) {
		res.status(200).json(
            'I am currently a 4th year Software Engineering Major at the '+
            'Rochester Institute of Technology (RIT). I have performed ' +
            'both front and back-end development for web-based systems. ' +
            'I take pride in creating exceptional software for people to ' +
            'use, enjoy, and add efficiency to their lives.'
        );
	};

	return responseFunctions;
};
