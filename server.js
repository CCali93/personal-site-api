const cluster = require('cluster');

var config = require('./config');

if (cluster.isMaster) {
	var cpuCount = require('os').cpus().length;

    // Create a worker for each CPU
    for (var i = 0; i < cpuCount; i += 1) {
        cluster.fork();
    }

	// Listen for dying workers
	cluster.on('exit', function (worker) {

	    // Replace the dead worker,
	    // we're not sentimental
	    console.log('Worker %d died :(', worker.id);
	    cluster.fork();

	});
} else {
	var app = require('./app')(config);

	app.listen(9001, function() {
		console.log('Example app listening on port 9000!');
	});
}
